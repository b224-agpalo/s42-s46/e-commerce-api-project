const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");


router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    console.log(userData)
    console.log(req.headers.authorization);

    userController.getProfile({ id: userData.id }).then(resultFromController => res.send(resultFromController))
});

router.post("/order", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin == false) {
        let data = {
            userId: userData.id,
            productId: req.body.productId
        }
        userController.order(data).then(resultFromController => res.send(resultFromController))
    }
    else { res.send("You are not a customer. Cannot order") }

});




module.exports = router;
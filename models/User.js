const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    orders: [
        {
            product: [{
                productId: {
                    type: String,
                    required: [true, "Object ID is required"]
                },
                productName: {
                    type: String,
                    required: [true, "Product name is required"]
                },
                quantity: {
                    type: Number,
                    default: 0
                },

            }],
            totalAmount: {
                type: Number,
                default: 0
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            }
        }

    ]

});

module.exports = mongoose.model("User", userSchema);
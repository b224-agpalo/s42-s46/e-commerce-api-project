const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const userController = require('../controllers/userController');
const auth = require("../auth");
const { response } = require("express");


router.post("/addProduct", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    productController.addProduct(userData.isAdmin, req.body).then(resultFromController => res.send(resultFromController))
});

router.get("/allProducts", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
});

router.get("/allActiveProducts", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    productController.getAllActiveProducts(userData).then(resultFromController => res.send(resultFromController))
});

router.get("/:productId", (req, res) => {
    console.log(req.params.productId)
    productController.getSpecificProducts(req.params).then(resultFromController => res.send(resultFromController))
});

router.put("/:productId", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    if (userData.isAdmin) { productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)) }
    else { res.send({ auth: "failed" }) }

});

router.put("/archive/:productId", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    if (userData.isAdmin) { productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)) }
    else { res.send({ auth: "failed" }) }

});
router.put("/activate/:productId", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    if (userData.isAdmin) { productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)) }
    else { res.send({ auth: "failed" }) }

});

module.exports = router;
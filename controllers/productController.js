const Product = require("../models/Product");

module.exports.addProduct = (userData, reqBody) => {
    if (userData == true) {
        let newProduct = new Product({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        });

        return newProduct.save().then((product, error) => {
            if (error) {
                return false
            } else {
                return true
            }
        })
    }
    else { return false }

};

module.exports.getSpecificProducts = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result
    })
};

module.exports.getAllActiveProducts = (data) => {
    if (data.isAdmin) {
        return Product.find({}).then(result => {
            return result

        })

    } else {
        return false
    }

};

module.exports.getAllProducts = (reqParams) => {
    return Product.find({}).then(result => {
        return result
    })
};

module.exports.updateProduct = (reqParams, reqBody) => {
    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((updatedProduct, error) => {

        console.log(updatedProduct)
        if (error) { return false }
        else { return true }

    }
    )


};

module.exports.archiveProduct = (reqParams, reqBody) => {
    let archivedProduct = {
        isActive: reqBody.isActive
    }
    return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((archivedProduct, error) => {

        console.log(archivedProduct)
        if (error) { return false }
        else { return true }

    }
    )


}
module.exports.activateProduct = (reqParams, reqBody) => {
    let activatedProduct = {
        isActive: reqBody.isActive
    }
    return Product.findByIdAndUpdate(reqParams.productId, activatedProduct).then((activatedProduct, error) => {

        console.log(activatedProduct)
        if (error) { return false }
        else { return TransformStreamDefaultController }

    }
    )


}
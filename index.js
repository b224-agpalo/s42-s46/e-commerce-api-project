const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");


const app = express();
const port = process.env.PORT || 4000;

mongoose.set("strictQuery", false);

mongoose.connect("mongodb+srv://jeff0818:Ryanjefferson013@batch224.1dappv1.mongodb.net/s42-s46?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }

);

let db = mongoose.connection;
db.on("error", () => console.error("Connection error"));
db.once("open", () => console.log("Connected to MongoDB."));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));



//Main URI
app.use("/users", userRoutes);
app.use("/products", productRoutes);



app.listen(port, () => { console.log(`API is now running at port: ${port}`) });